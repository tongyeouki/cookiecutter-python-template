## Readme

{{ cookiecutter.project_short_description }}

### Presentation

#### Who?

#### What?

#### When?

#### How?

### Install

For development:

    $ make development

For production

    $ make install

### Tests

    $ make test

To check coverage:

    $ make coverage

### Documentation

    $ make docgen

#### Contributing

Check dedicated documentation in the `.gitlab` folder.

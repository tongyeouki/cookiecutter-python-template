# Installation


## Stable release

To install {{ cookiecutter.project_name }}, run this command in your terminal:

.. code-block:: console

    $ make install
    $ make install-dev

This is the preferred method to install {{ cookiecutter.project_name }}, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


## From sources

The sources for {{ cookiecutter.project_name }} can be downloaded from the `gitlab repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone git://gitlab.com/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.gitlab_slug }}

Or download the `tarball`_:

.. code-block:: console

    $ curl  -OL https://gitlab.com/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.gitlab_slug }}/tarball/master

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _gitlab repo: https://gitlab.com/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}
.. _tarball: https://gitlab.com/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}/tarball/master

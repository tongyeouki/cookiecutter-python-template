## Issue Template

* {{ cookiecutter.project_code }} version:
* Python version:
* Operating System:

### Description

Describe what you were trying to get done.
Tell us what happened, what went wrong, and what you expected to happen.

### Expected behaviour

What is expected.

### Actual behaviour

Describe the program behaviour.

### How to reproduce the bug ?

```
Paste the command(s) you ran and the output.
If there was a crash, please include the traceback here.
```

import pytest

from src.{{cookiecutter.project_slug}}.settings.base import get_config


@pytest.fixture()
def config():
    return get_config("config.sample.yaml")


def test_file_absolute_path(config):
    path = config.get_file_path()
    assert "settings/config.sample.yaml" in path


def test_should_get_content_from_file(config):
    content = config.get_file_content()
    expected = {'test': True}
    assert content == expected


def test_get_item(config):
    result = config.get["test"]
    assert result is True


def test_get_wrong_item(config):
    with pytest.raises(KeyError):
        config.get["whatever"]


def test_file_does_not_exist_shoult_raise_exception():
    with pytest.raises(FileNotFoundError):
        get_config("whatever.yaml").get["whatever"]


def test_should_print_repr_for_config(config):
    assert repr(config) == "<Configuration: config.sample.yaml>"

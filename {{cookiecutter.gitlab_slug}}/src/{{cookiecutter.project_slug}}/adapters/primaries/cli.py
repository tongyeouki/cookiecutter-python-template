import sys
import click


@click.command()
def cli(args=None):
    """Console script for {{cookiecutter.project_slug}}."""
    click.echo("Hello World!")
    click.echo("See click documentation at http://click.pocoo.org/")
    return 0


if __name__ == "__main__":
    sys.exit(cli())  # pragma: no cover
